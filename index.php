<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Publicando.me sua plataforma de consumo textual sem drm!</title>

    <meta name="description" content="Source code generated using layoutit.com">
    <meta name="author" content="LayoutIt!">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

  </head>
  <body>

    <div class="container-fluid">
	<div class="row">
		<div class="col-md-12">

      <!-- Navbar -->
			<nav class="navbar navbar-default navbar-fixed-top navbar-inverse" role="navigation">
				<div class="navbar-header">

					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						 <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
					</button> <a class="navbar-brand" href="#"><img src='imgs/brand.png'></a>
				</div>

				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li>
							<a href="#">Home</a>
						</li>
						<li>
							<a href="#">Sobre</a>
						</li>
						<li class="dropdown">
							 <a href="#" class="dropdown-toggle" data-toggle="dropdown">Categorias<strong class="caret"></strong></a>
							<ul class="dropdown-menu">
								<li>
									<a href="#">Creative Commons (Free)</a>
								</li>
								<li>
									<a href="#">Creative Commons (Paid)</a>
								</li>
                <li class="divider">
								</li>
								<li>
									<a href="#">Copyright (Free)</a>
								</li>
								<li>
									<a href="#">Copyright (Paid)</a>
								</li>
							</ul>
						</li>
					</ul>

					<ul class="nav navbar-nav navbar-right">
            <li>
              <form class="navbar-form navbar-right" role="search">
    						<div class="form-group">
    							<input type="text" class="form-control">
    						</div>
    						<button type="submit" class="btn btn-default">
    							Pesquisar
    						</button>
    					</form>
            </li>
						<li>
							<a href="#">LogIn/SignIn</a>
						</li>
					</ul>
				</div>

			</nav>
      <!-- Navbar -->

      <!-- conteúdo e gambiarras zoeiras -->

      <br />
      <br />
      <br />
      <br />

			<h3>
				Novidades
			</h3>

			<div class="carousel slide" id="carousel-427448">
				<ol class="carousel-indicators">
					<li class="active" data-slide-to="0" data-target="#carousel-427448">
					</li>
					<li data-slide-to="1" data-target="#carousel-427448">
					</li>
				</ol>
				<div class="carousel-inner">
					<div class="item active">
						<img alt="Carousel Bootstrap First" src="http://lorempixel.com/output/sports-q-c-1600-500-1.jpg">
						<div class="carousel-caption">
							<h4>
								Livro estrobofosfólico bizarramente fodônico zica do úniverso tangivel e intangivel.
							</h4>
							<p>
								Xablau é o que define a obra! Clique, aproveite que é um livro sob Creative Commons e de distribuição gratuíta!
							</p>
						</div>
					</div>
					<div class="item">
						<img alt="Carousel Bootstrap Second" src="http://lorempixel.com/output/sports-q-c-1600-500-2.jpg">
						<div class="carousel-caption">
							<h4>
								Livro estrobofosfólico bizarramente fodônico zica do úniverso tangivel e intangivel.
							</h4>
							<p>
								Xablau é o que define a obra! Clique e faça a compra do título! Aproveite as diversas opções de pagamento!
							</p>
						</div>
					</div>
				</div> <a class="left carousel-control" href="#carousel-427448" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a> <a class="right carousel-control" href="#carousel-427448" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
			</div>
		</div>
	</div>

  <br />
  <br />
  <br />
  <br />

	<div class="row">
		<div class="col-md-4">
			<div class="media">
				 <a href="#" class="pull-left"><img alt="Bootstrap Media Preview" src="http://lorempixel.com/64/64/" class="media-object"></a>
				<div class="media-body">
					<h4 class="media-heading">
						Sinopse boladona
					</h4> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.
				</div>
			</div>
		</div>
    <div class="col-md-4">
			<div class="media">
				 <a href="#" class="pull-left"><img alt="Bootstrap Media Preview" src="http://lorempixel.com/64/64/" class="media-object"></a>
				<div class="media-body">
					<h4 class="media-heading">
						Sinopse boladona
					</h4> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.
				</div>
			</div>
		</div>
    <div class="col-md-4">
			<div class="media">
				 <a href="#" class="pull-left"><img alt="Bootstrap Media Preview" src="http://lorempixel.com/64/64/" class="media-object"></a>
				<div class="media-body">
					<h4 class="media-heading">
						Sinopse boladona
					</h4> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.
				</div>
			</div>
		</div>
	</div>

  <br />
  <br />
  <br />
  <br />

	<div class="row">
		<div class="col-md-12">
  		<footer>
      	<div class="panel panel-default">
  				<div class="panel-body">
  					Informações extras que não pensei ainda quais serão.
  				</div>
  			</div>
      </footer>
		</div>
	</div>
</div>

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
  </body>
</html>
